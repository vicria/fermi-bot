sources = [
    {
        'name': 'yandex',
        'link': 'https://realty.yandex.ru/sankt-peterburg/kupit/kvartira/CAT1/?newFlat=NO&showSimilar=NO&apartments=NO&CAT2&CAT3',
        'categories': [
            {
                'name': 'rooms',
                'values': [
                    'studiya',
                    'odnokomnatnaya',
                    'dvuhkomnatnaya',
                    'tryohkomnatnaya',
                    'chetyryohkomnatnaya',
                    'pyatikomnatnaya',
                    'shestikomnatnaya',
                    '7-i-bolee',
                    'svobodnaya-planirovka',
                ]

            },
            {
                'name': 'floor',
                'values': [
                    'floorMin=1&floorMax=1',
                    'floorMin=2&floorMax=2',
                    'floorMin=3&floorMax=3',
                    'floorMin=4&floorMax=4',
                    'floorMin=5&floorMax=5',
                    'floorMin=6&floorMax=6',
                    'floorMin=7&floorMax=7',
                    'floorMin=8&floorMax=8',
                    'floorMin=9&floorMax=9',
                    'floorMin=10&floorMax=10',
                    'floorMin=11&floorMax=11',
                    'floorMin=12&floorMax=12',
                    'floorMin=13&floorMax=13',
                    'floorMin=14&floorMax=14',
                    'floorMin=15&floorMax=15',
                    'floorMin=16&floorMax=16',
                    'floorMin=17'
                ]
            },
            {
                'name': 'area',
                'values': [
                    'areaMax=20',
                    'areaMin=21&areaMax=25',
                    'areaMin=26&areaMax=30',
                    'areaMin=31&areaMax=40',
                    'areaMin=41&areaMax=50',
                    'areaMin=51&areaMax=60',
                    'areaMin=61&areaMax=70',
                    'areaMin=71',
                ]
            }
        ]
    }
]

# https://spb.cian.ru/kupit-kvartiru-vtorichka/