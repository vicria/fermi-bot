###### AVITO GET DATA
import config
from selenium import webdriver
from selenium import common
from selenium.webdriver.common.by import By
import time

#driver = webdriver.Chrome()

def download_to_folder(link, path, filename):
    print('Start processing: ' + link)
    print('Filename: ' + filename)

    is_there_next_page = True
    page_number = 0
    next_page = link
    while (is_there_next_page):
        driver.get(next_page)
        captcha = True
        while captcha:
            title = driver.find_element_by_tag_name('title')
            title_text = title.get_attribute('text')
            if (title_text == 'Ой!'):
                time.sleep(2)
                print('captcha found!')
            else:
                captcha = False
        content = driver.page_source
        with open(path + filename + ' ' +  str(page_number + 1) + '.html', 'w', encoding='utf8') as output_file:
            output_file.write(content)
            #time.sleep(1)

            try:
                elem = driver.find_element_by_xpath("//a[contains(text(),'Следующая страница →')]")
                print('there is Next link')
                page_number += 1
                next_page = link + '&page=' + str(page_number + 1)
            except common.exceptions.NoSuchElementException:
                print('there is no Next link')
                is_there_next_page = False
            print('finish link iterations')
    #driver.close()

def avito_get_data():
    avito_searches = [
       {
            'name': 'Студия',
            'link': 'https://www.avito.ru/sankt-peterburg/kvartiry/prodam/studii/vtorichka?f=59_0b0.496_0b0.497_0b0&s_trg=4'
        },
        {
            'name': '1комн',
            'link': 'https://www.avito.ru/sankt-peterburg/kvartiry/prodam/1-komnatnye/vtorichka?f=59_0b0.496_0b0.497_0b0&s_trg=4'
         },
        {
            'name': '2комн этаж 1-10',
            'link': 'https://www.avito.ru/sankt-peterburg/kvartiry/prodam/2-komnatnye/vtorichka?f=59_0b0.496_0b5129.497_0b0'
         },
        {
            'name': '2комн этаж 11+ ',
            'link': 'https://www.avito.ru/sankt-peterburg/kvartiry/prodam/2-komnatnye/vtorichka?s_trg=4&f=59_0b0.496_5130b0.497_0b0'
         },
             {
            'name': '3комн этаж 1-7',
            'link': 'https://www.avito.ru/sankt-peterburg/kvartiry/prodam/3-komnatnye/vtorichka?f=59_0b0.496_0b0.497_0b5188&s_trg=4'
         },
         {
            'name': '3комн этаж 8+',
            'link': 'https://www.avito.ru/sankt-peterburg/kvartiry/prodam/3-komnatnye/vtorichka?f=59_0b0.496_5127b0.497_0b0&s_trg=4'
         },
         {
            'name': '#4+ комн',
            'link': 'https://www.avito.ru/sankt-peterburg/kvartiry/prodam/vtorichka?f=549_5699-5700-5701-11018-11019-11020-11021.59_0b0.496_0b0.497_0b0&s_trg=4'
         }
    ]

    for search in avito_searches:
        search_name = search['name']
        search_link = search['link']
        download_to_folder(search_link, 'data\\avito\\', search_name)

###### AVITO PARSE LOCAL
import os
from bs4 import BeautifulSoup


def flat_html_parser(element_html):
    flt_id = None
    flt_link = None
    flt_area = None
    flt_rooms_amount = None
    flt_price = None
    flt_floor = None
    flt_floors_in_house = None
    flt_adress = None
    flt_subway = None
    flt_distance = None

    link_html = element_html.find_all('a', {'class': 'item-description-title-link'})
    flt_link = 'https://www.avito.ru' + link_html[0]['href']
    flt_id = flt_link.split('_')[-1]
    flt_title_html = link_html[0]['title']
    # 6-к квартира, 152 м², 2/5 эт. в Санкт-Петербурге
    flt_rooms_amount = flt_title_html.split(', ')[0]
    flt_area = flt_title_html.split(', ')[1].split(' ')[0]
    flt_floor_text = flt_title_html.split(', ')[2].split('эт.')[0]
    flt_floor = flt_floor_text.split('/')[0]
    flt_floors_in_house = flt_floor_text.split('/')[1]
    # < span class ="price price_highlight" content="13800000" data-marker="item-price" itemprop="price" >
    price_html = element_html.find_all('span', {'class': 'price'})
    flt_price = price_html[0]['content']
    #  <p class="address" data-marker="item-address">...
    # Выборгская <span class="c-2">700 м</span>, Санкт-Петербург, Большой Сампсониевский проспект, 25 </p>
    adress_html = element_html.find_all('p', {'data-marker': 'item-address'})
    flt_adress_line = adress_html[0].text.strip()
    # Выборгская 700 м, Санкт-Петербург, Большой Сампсониевский проспект, 25
    # Старая деревня 3.6 км, Санкт-Петербург, Большой Сампсониевский проспект, 25
    flt_subway = ' '.join(flt_adress_line.split(',')[0].split(' ')[:-2])
    flt_distance = ' '.join(flt_adress_line.split(', ')[0].split(' ')[-2:])
    flt_adress = ', '.join(flt_adress_line.split(', ')[1:])

    flt_adress = flt_adress.replace('\xa0', '. ')

    if flt_rooms_amount == 'Студия':
        flt_rooms_amount = 0
    elif flt_rooms_amount[1:] == '-к квартира':
        flt_rooms_amount = flt_rooms_amount[0]
    elif flt_rooms_amount == '> 9-к квартира':
        flt_rooms_amount = 9

    ## 'Ленинградская область' ans so on in distance field when no subway
    if flt_subway == '':
        flt_subway = None
        flt_distance = None

    return (
        flt_id,
        flt_link,
        flt_area,
        flt_rooms_amount,
        flt_price,
        flt_floor,
        flt_floors_in_house,
        flt_adress,
        flt_subway,
        flt_distance
    )


def avito_parse_local(path):

    from sqlalchemy import create_engine
    from sqlalchemy import Table, Column, String, MetaData
    db_string = "postgresql://postgres:admin@localhost/postgres"
    db = create_engine(db_string)
    meta = MetaData(db)
    flats2_table = Table('flats', meta,
                         Column('id', String),
                         Column('link', String),
                         Column('area', String),
                         Column('rooms_amount', String),
                         Column('price', String),
                         Column('floor', String),
                         Column('floors_in_house', String),
                         Column('address', String),
                         Column('subway_station', String),
                         Column('subway_distance', String),
                         Column('is_agent', String))

    files = os.listdir(path)
    for file in files:
        with open(path + file, 'r', encoding='utf8') as fp:
            soup = BeautifulSoup(fp, 'lxml')
            flat_htmls = soup.find_all('div', {'class': 'item_table-wrapper'})
            for flat_html in flat_htmls:
                tuple = flat_html_parser(flat_html)
                f_id, f_link, f_area, f_rooms_amount, f_price, f_floor, f_floors_in_house, f_adress, f_subway, f_distance = tuple
                with db.connect() as conn:
                    insert_statement = flats2_table.insert().values(
                        id=f_id,
                        link=f_link,
                        area=f_area,
                        rooms_amount=f_rooms_amount,
                        price=f_price,
                        floor=f_floor,
                        floors_in_house=f_floors_in_house,
                        address=f_adress,
                        subway_station=f_subway,
                        subway_distance=f_distance
                    )
                    conn.execute(insert_statement)

###### MAIN
#avito_get_data()
avito_parse_local('C:\\Work\\avito_it03\\')

